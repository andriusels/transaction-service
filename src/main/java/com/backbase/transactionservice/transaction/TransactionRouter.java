package com.backbase.transactionservice.transaction;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class TransactionRouter extends RouteBuilder {

    private static final String OPEN_BANK_URI;

    static {
        OPEN_BANK_URI = "apisandbox.openbankproject.com/obp/v1.2.1/banks/rbs/accounts/${header.account}/public/transactions";
    }

    @Override
    public void configure() {
        from("direct:fetchTransactionsFromOpenBank")
                .recipientList(simple("https4:" + OPEN_BANK_URI))
                .process(new TransactionProcessor());

    }
}