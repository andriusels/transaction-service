package com.backbase.transactionservice.controller;

import com.backbase.transactionservice.model.TotalSum;
import com.backbase.transactionservice.model.Transaction;
import com.backbase.transactionservice.transaction.GetTransactions;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/transactions/{account}")
public class TransactionController {

    private final GetTransactions getTransactions;

    @Autowired
    public TransactionController(GetTransactions getTransactions) {
        this.getTransactions = getTransactions;
    }

    @GetMapping()
    public List<Transaction> get(@PathVariable(value = "account") String account) {
        return getTransactions.getAll(account);
    }

    @GetMapping("/filter/{type}")
    public List<Transaction> filter(@PathVariable(value = "account") String account,
                                    @PathVariable(value = "type") String type) {
        return getTransactions.getAll(account, type);
    }

    @GetMapping("/sum/{type}")
    public TotalSum sum(@PathVariable(value = "account") String account,
                        @PathVariable(value = "type") String type) {
        return new TotalSum(getTransactions.sum(account, type));
    }
}
