package com.backbase.transactionservice.json;

import com.backbase.transactionservice.model.Transaction;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.Assert.*;

public class TransactionDeserializerTest {

    @Test
    public void deserialize() throws IOException {
        ObjectMapper om = new ObjectMapper();

        final Transaction transaction = om.readValue("{\"id\":\"897706c1-dcc6-4e70-9d85-8a537c7cbf3e\",\"this_account\":{\"id\":\"savings-kids-john\",\"holders\":[{\"name\":\"Savings - Kids John\",\"is_alias\":false}],\"number\":\"832425-00304050\",\"kind\":\"savings\",\"IBAN\":null,\"swift_bic\":null,\"bank\":{\"national_identifier\":\"rbs\",\"name\":\"The Royal Bank of Scotland\"}},\"other_account\":{\"id\":\"E806HT1hp-IfBH0DP1rCFrPAftEtpCAwU-XlMo_9bzM\",\"holder\":{\"name\":\"ALIAS_49532E\",\"is_alias\":true},\"number\":\"savings-kids-john\",\"kind\":null,\"IBAN\":null,\"swift_bic\":null,\"bank\":{\"national_identifier\":\"rbs\",\"name\":\"rbs\"},\"metadata\":{\"public_alias\":null,\"private_alias\":null,\"more_info\":null,\"URL\":null,\"image_URL\":null,\"open_corporates_URL\":null,\"corporate_location\":null,\"physical_location\":null}},\"details\":{\"type\":\"SANDBOX_TAN\",\"description\":\"Gift\",\"posted\":\"2017-10-15T14:22:28Z\",\"completed\":\"2017-10-15T14:22:28Z\",\"new_balance\":{\"currency\":\"GBP\",\"amount\":null},\"value\":{\"currency\":\"GBP\",\"amount\":\"-90.00\"}},\"metadata\":{\"narrative\":null,\"comments\":[],\"tags\":[],\"images\":[],\"where\":null}}", Transaction.class);
        assertEquals("897706c1-dcc6-4e70-9d85-8a537c7cbf3e", transaction.getId());
        assertEquals("savings-kids-john", transaction.getAccountId());
        assertEquals("savings-kids-john", transaction.getCounterpartyAccount());
        assertEquals("ALIAS_49532E", transaction.getCounterpartyName());
        assertEquals("null", transaction.getCounterPartyLogoPath());
        assertEquals(new BigDecimal(-90.00).setScale(2), transaction.getInstructedAmount());
        assertEquals("GBP", transaction.getInstructedCurrency());
        assertEquals(new BigDecimal(-90.00).setScale(2), transaction.getTransactionAmount());
        assertEquals("GBP", transaction.getTransactionCurrency());
        assertEquals("SANDBOX_TAN", transaction.getTransactionType());
        assertEquals("Gift", transaction.getDescription());
    }
}