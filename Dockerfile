FROM openjdk:8-jdk-alpine
EXPOSE 80
VOLUME /tmp
ADD /target/transaction-service-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]