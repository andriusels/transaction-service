package com.backbase.transactionservice.transaction;

import com.backbase.transactionservice.model.TransactionWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.io.IOException;

public class TransactionProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        String jsonString = exchange.getIn().getBody(String.class);

        ObjectMapper mapper = new ObjectMapper();
        try {
            TransactionWrapper result = mapper.readValue(jsonString, TransactionWrapper.class);
            exchange.getIn().setBody(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
