package com.backbase.transactionservice.json;

import com.backbase.transactionservice.model.Transaction;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers;

import java.io.IOException;
import java.math.BigDecimal;

public class TransactionDeserializer extends JsonDeserializer<Transaction> {

    @Override
    public Transaction deserialize(JsonParser parser, DeserializationContext ctx) throws IOException {
        Transaction transaction = new Transaction();
        ObjectCodec codec = parser.getCodec();
        try {
            JsonNode node = codec.readTree(parser);
            transaction.setId(node.get("id").asText());
            transaction.setAccountId(node.get("this_account").get("id").asText());
            transaction.setCounterpartyAccount(node.get("other_account").get("number").asText());
            transaction.setCounterpartyName(node.get("other_account").get("holder").get("name").asText());
            transaction.setCounterPartyLogoPath(node.get("other_account").get("metadata").get("image_URL").asText());
            transaction.setInstructedAmount(parseDecimal(node.get("details").get("value").get("amount")));
            transaction.setInstructedCurrency(node.get("details").get("value").get("currency").asText());
            transaction.setTransactionAmount(parseDecimal(node.get("details").get("value").get("amount")));
            transaction.setTransactionCurrency(node.get("details").get("value").get("currency").asText());
            transaction.setTransactionType(node.get("details").get("type").asText());
            transaction.setDescription(node.get("details").get("description").asText());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return transaction;
    }

    private BigDecimal parseDecimal(JsonNode node) {
        return new BigDecimal(node.asText()).setScale(2, BigDecimal.ROUND_HALF_UP);
    }
}