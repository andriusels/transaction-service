package com.backbase.transactionservice.transaction;

import com.backbase.transactionservice.model.Transaction;
import com.backbase.transactionservice.model.TransactionWrapper;
import org.apache.camel.ProducerTemplate;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class GetTransactionsTest {

    private GetTransactions getTransactions;
    private ProducerTemplate producerTemplate;

    @Before
    public void setUp() throws Exception {
        producerTemplate = Mockito.mock(ProducerTemplate.class);
        getTransactions = new GetTransactions(producerTemplate);
    }

    @Test
    public void getAll() {
        when(producerTemplate.requestBodyAndHeader("direct:fetchTransactionsFromOpenBank",
                null,
                "account",
                "junit",
                TransactionWrapper.class))
                .thenReturn(transactionWrapper());
        assertEquals(3, getTransactions.getAll("junit").size());
    }

    @Test
    public void sum() {
        final TransactionWrapper t = transactionWrapper();
        t.getTransactions().add(new Transaction("junit", new BigDecimal(17.43)));
        when(producerTemplate.requestBodyAndHeader("direct:fetchTransactionsFromOpenBank",
                null,
                "account",
                "junit",
                TransactionWrapper.class))
                .thenReturn(t);
        assertEquals(new BigDecimal(17.43), getTransactions.sum("junit", "junit"));
    }

    @Test
    public void getAll1() {
        final TransactionWrapper t = transactionWrapper();
        t.getTransactions().add(new Transaction("junit", new BigDecimal(17.43)));
        when(producerTemplate.requestBodyAndHeader("direct:fetchTransactionsFromOpenBank",
                null,
                "account",
                "junit",
                TransactionWrapper.class))
                .thenReturn(t);
        assertEquals(1, getTransactions.getAll("junit", "junit").size());
    }

    private TransactionWrapper transactionWrapper() {
        final TransactionWrapper t = new TransactionWrapper();
        t.getTransactions().add(new Transaction("rndm", BigDecimal.TEN));
        t.getTransactions().add(new Transaction("rndm", BigDecimal.TEN));
        t.getTransactions().add(new Transaction("rndm", BigDecimal.TEN));
        return t;
    }

}