package com.backbase.transactionservice.model;

import com.backbase.transactionservice.json.MoneySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;

public class TotalSum {

    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal totalAmount;

    public TotalSum(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }
}
