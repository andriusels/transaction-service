package com.backbase.transactionservice.transaction;

import com.backbase.transactionservice.model.TransactionWrapper;
import org.apache.camel.ProducerTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TransactionRouterIT {

    @Autowired
    private ProducerTemplate producerTemplate;

    @Test
    public void getBody() {
        final TransactionWrapper transactionWrapper
                = producerTemplate.requestBodyAndHeader("direct:fetchTransactionsFromOpenBank",
                null,
                "account",
                "savings-kids-john",
                TransactionWrapper.class);
        assertFalse(transactionWrapper.getTransactions().isEmpty());
    }
}