package com.backbase.transactionservice.transaction;

import com.backbase.transactionservice.model.Transaction;
import com.backbase.transactionservice.model.TransactionWrapper;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GetTransactions {

    private final ProducerTemplate producerTemplate;

    @Autowired
    public GetTransactions(ProducerTemplate producerTemplate) {
        this.producerTemplate = producerTemplate;
    }

    public List<Transaction> getAll(String account, String type) {
        return getAll(account).stream()
                .filter(t -> t.getTransactionType().equals(type))
                .collect(Collectors.toList());
    }

    public BigDecimal sum(String account, String type) {
        return getAll(account, type).stream()
                .map(Transaction::getTransactionAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public List<Transaction> getAll(String account) {
        final TransactionWrapper transactionWrapper
                = producerTemplate.requestBodyAndHeader("direct:fetchTransactionsFromOpenBank",
                null,
                "account",
                account,
                TransactionWrapper.class);
        return transactionWrapper.getTransactions();
    }

}
