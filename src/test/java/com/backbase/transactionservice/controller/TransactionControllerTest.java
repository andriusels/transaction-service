package com.backbase.transactionservice.controller;

import com.backbase.transactionservice.transaction.GetTransactions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.verify;

public class TransactionControllerTest {

    private TransactionController controller;
    private GetTransactions getTransactions;

    @Before
    public void setUp() throws Exception {
        getTransactions = Mockito.mock(GetTransactions.class);
        controller = new TransactionController(getTransactions);
    }

    @Test
    public void get() {
        controller.get("junit");
        verify(getTransactions).getAll("junit");
    }

    @Test
    public void filter() {
        controller.filter("junit", "type");
        verify(getTransactions).getAll("junit", "type");
    }

    @Test
    public void sum() {
        controller.sum("junit", "type");
        verify(getTransactions).sum("junit", "type");
    }
}