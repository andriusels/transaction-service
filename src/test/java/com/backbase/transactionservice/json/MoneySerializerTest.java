package com.backbase.transactionservice.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class MoneySerializerTest {

    @Test
    public void serialize() throws JsonProcessingException {
        MoneyWrapper t1 = new MoneyWrapper(new BigDecimal(0));
        MoneyWrapper t2 = new MoneyWrapper(new BigDecimal(7.00654686));
        MoneyWrapper t3 = new MoneyWrapper(new BigDecimal(12.1));
        MoneyWrapper t4 = new MoneyWrapper(new BigDecimal(21.1));
        MoneyWrapper t5 = new MoneyWrapper(new BigDecimal(47.003783518));

        ObjectMapper om = new ObjectMapper();
        assertEquals("{\"money\":\"0.00\"}", om.writeValueAsString(t1));
        assertEquals("{\"money\":\"7.01\"}", om.writeValueAsString(t2));
        assertEquals("{\"money\":\"12.10\"}", om.writeValueAsString(t3));
        assertEquals("{\"money\":\"21.10\"}", om.writeValueAsString(t4));
        assertEquals("{\"money\":\"47.00\"}", om.writeValueAsString(t5));
    }

    class MoneyWrapper {

        @JsonSerialize(using = MoneySerializer.class)
        private BigDecimal money;

        public MoneyWrapper() {
        }

        MoneyWrapper(BigDecimal money) {
            this.money = money;
        }

        public void setMoney(BigDecimal money) {
            this.money = money;
        }

        public BigDecimal getMoney() {
            return money;
        }
    }
}